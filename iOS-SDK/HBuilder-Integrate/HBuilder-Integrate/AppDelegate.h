//
//  AppDelegate.h
//  Pandora
//
//  Created by Mac Pro_C on 12-12-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ConnectViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) ConnectViewController *viewController;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController * navigationController;
@property(assign) BOOL pageTransition;
@end
