//
//  PluginPrinter.h
//  HBuilder-Integrate
//
//  Created by muomeng on 12/25/15.
//  Copyright © 2015 DCloud. All rights reserved.
//

#include "PGPlugin.h"
#include "PGMethod.h"
#import "MyPeripheral.h"
#import "CBController.h"
#import "Printer.h"
#import "ReliableBurstData.h"
#define BM77SPP_HW @"353035305f535050"
#define BM77SPP_FW @"32303233303230"
#define BM77SPP_AD 0xd97e

#define BLETR_HW @"353035305f424c455452"
#define BLETR_FW @"32303032303130"
#define BLETR_AD 0x4833

@interface NSString (HexString)
+ (NSString *)stringFromHexString:(NSString *)hexString;
@end

@implementation NSString(HexString)

+ (NSString *)stringFromHexString:(NSString *)hexString {
    
    // The hex codes should all be two characters.
    if (([hexString length] % 2) != 0)
        return nil;
    
    NSMutableString *string = [NSMutableString string];
    
    for (NSInteger i = 0; i < [hexString length]; i += 2) {
        
        NSString *hex = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSInteger decimalValue = 0;
        sscanf([hex UTF8String], "%x", &decimalValue);
        [string appendFormat:@"%c", decimalValue];
    }
    
    return string;
}

@end

@interface PGPluginPrinter : PGPlugin<MyPeripheralDelegate,ReliableBurstDataDelegate>{
    NSString *_hardwareRevision;
    NSString *_firmwareRevision;
    BOOL isPatch;
    NSMutableArray *_loopBackQueue;
    NSTimer *_loopBackTimer;
}


@property(nonatomic, strong)MyPeripheral * connectedPeripheral;
@property(nonatomic, strong)Printer *printer;
- (void)PluginPrinterFunction:(PGMethod*)commands;
- (void)PluginPrinterFunctionArrayArgu:(PGMethod*)commands;

- (NSData*)PluginPrinterFunctionSync:(PGMethod*)commands;
- (NSData*)PluginPrinterFunctionSyncArrayArgu:(PGMethod*)commands;
@end
