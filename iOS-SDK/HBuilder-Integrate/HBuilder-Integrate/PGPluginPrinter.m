//
//  PluginPrinter.m
//  HBuilder-Integrate
//
//  Created by muomeng on 12/25/15.
//  Copyright © 2015 DCloud. All rights reserved.
//

#import "PGPluginPrinter.h"
#import "ConnectViewController.h"
#import "AppDelegate.h"


@implementation PGPluginPrinter
- (void)PluginPrinterFunction:(PGMethod*)commands
{
    if ( commands ) {
        
        // CallBackid 异步方法的回调id，H5+ 会根据回调ID通知JS层运行结果成功或者失败
        NSString* cbId = [commands.arguments objectAtIndex:0];
        
        // 用户的参数会在第二个参数传回
        NSString* pArgument1 = [commands.arguments objectAtIndex:1];
        NSString* pArgument2 = [commands.arguments objectAtIndex:2];
        NSString* pArgument3 = [commands.arguments objectAtIndex:3];
        NSString* pArgument4 = [commands.arguments objectAtIndex:4];
        
        // 如果使用Array方式传递参数
        NSArray* pResultString = [NSArray arrayWithObjects:pArgument1, pArgument2, pArgument3, pArgument4, nil];
        
        // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsArray: pResultString];
        
        // 如果Native代码运行结果和预期不同，需要通过回调通知JS层出现错误，并返回错误提示
        //PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusError messageAsString:@"惨了! 出错了！ 咋(wu)整(liao)"];
        
        // 通知JS层Native层运行结果
        [self toCallback:cbId withReslut:[result toJSONString]];
        [self showConnectionView];
        
    }
}

- (void)PluginPrinterFunctionArrayArgu:(PGMethod*)commands
{
    if ( commands ) {
        
        // CallBackid 异步方法的回调id，H5+ 会根据回调ID通知JS层运行结果成功或者失败
        NSString* cbId = [commands.arguments objectAtIndex:0];
        
        // 用户的参数会在第二个参数传回，可以按照Array方式传入，
        NSArray* pArray = [commands.arguments objectAtIndex:1];
        
        // 如果使用Array方式传递参数
        NSString* pResultString = [NSString stringWithFormat:@"%@ %@ %@ %@",[pArray objectAtIndex:0], [pArray objectAtIndex:1], [pArray objectAtIndex:2], [pArray objectAtIndex:3]];
        
        // 运行Native代码结果和预期相同，调用回调通知JS层运行成功并返回结果
        PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusOK messageAsString:pResultString];
        
        // 如果Native代码运行结果和预期不同，需要通过回调通知JS层出现错误，并返回错误提示
        //PDRPluginResult *result = [PDRPluginResult resultWithStatus:PDRCommandStatusError messageAsString:@"惨了! 出错了！ 咋(wu)整(liao)"];
        
        // 通知JS层Native层运行结果
        [self toCallback:cbId withReslut:[result toJSONString]];
        
        _printer=[[Printer alloc]init];
        _connectedPeripheral.deviceInfoDelegate = self;
        _connectedPeripheral.proprietaryDelegate = self;
        if (isPatch) {
            [_connectedPeripheral setTransDataNotification:TRUE];
        }
        else {
            [_connectedPeripheral readHardwareRevision];
        }
        _connectedPeripheral.transmit.delegate = self;
        [_printer setPort:_connectedPeripheral];
    
        
        
        
        [self print:pResultString];
       
        if (([_connectedPeripheral transparentDataWriteChar] == nil) || ([_connectedPeripheral transparentDataReadChar] == nil) ) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Service Not Found" message:@"Can't find custom service UUID or TX/RX UUID" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alertView show];

            [self showConnectionView];
        }
        

    }
}


- (NSData*)PluginPrinterFunctionSync:(PGMethod*)command
{
    // 根据传入获取参数
    NSString* pArgument1 = [command.arguments objectAtIndex:0];
    NSString* pArgument2 = [command.arguments objectAtIndex:1];
    NSString* pArgument3 = [command.arguments objectAtIndex:2];
    NSString* pArgument4 = [command.arguments objectAtIndex:3];
    
    // 拼接成字符串
    NSString* pResultString = [NSString stringWithFormat:@"%@ %@ %@ %@", pArgument1, pArgument2, pArgument3, pArgument4];
    
    // 按照字符串方式返回结果
    return [self resultWithString: pResultString];
}


- (NSData*)PluginPrinterFunctionSyncArrayArgu:(PGMethod*)command
{
    // 根据传入参数获取一个Array，可以从中获取参数
    NSArray* pArray = [command.arguments objectAtIndex:0];
    
    // 创建一个作为返回值的NSDictionary
    NSDictionary* pResultDic = [NSDictionary dictionaryWithObjects:pArray forKeys:[NSArray arrayWithObjects:@"RetArgu1",@"RetArgu2",@"RetArgu3", @"RetArgu4", nil]];
    
    // 返回类型为JSON，JS层在取值是需要按照JSON进行获取
    return [self resultWithJSON: pResultDic];
}

-(void)showConnectionView{
    ConnectViewController * connectionvct =  [[ConnectViewController alloc] initWithNibName:@"ConnectViewController" bundle:nil];
    
    
    connectionvct.blockCallback = ^(MyPeripheral *myPeripheral){
        self.connectedPeripheral = myPeripheral;
        NSLog(@"---%@",self.connectedPeripheral.advName);
    };
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:connectionvct];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.navigationController = nav;
    [self presentViewController:nav animated:YES completion:^{
        
    }];

}


- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *)error {
    NSLog(@"[DataTransparentViewController] didSendTransparentDataStatus");
    if (error == nil) {
        if (_printer.sendFlag) {
            [NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];
        }
    }
}
- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data {
    NSLog(@"[DataTransparentViewController] didReceiveTransparentData");
    
}
- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify{
    NSLog(@"DataTransparentViewController] didUpdateTransDataNotifyStatus = %@",notify==true?@"true":@"false");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"Hardware = %@",hardwareRevision);
        _hardwareRevision = [hardwareRevision retain];
        if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]]) {
            //BM77SPP
            [_connectedPeripheral readFirmwareRevision];
        }
        else if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]]) {
            //BLETR
            [_connectedPeripheral readFirmwareRevision];
        }
        else {
            isPatch = YES;
            [_connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"firmwareRevision = %@",firmwareRevision);
        _firmwareRevision = [firmwareRevision retain];
        if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]]) {
            //BM77SPP
            [_connectedPeripheral readMemoryValue:BM77SPP_AD length:2];
        }
        else if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]]) {
            //BLETR
            [_connectedPeripheral readMemoryValue:BLETR_AD length:2];
        }
        else {
            isPatch = YES;
            [_connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data {
    NSLog(@"%@ = %@",address,data);
    
    unsigned short int add;
    [address getBytes:&add length:2];
    add = NSSwapBigShortToHost(add);
    unsigned short int d;
    [data getBytes:&d length:length];
    d = NSSwapBigShortToHost(d);
    if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]] && add == BM77SPP_AD) {
        if (d != 0x0017) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [_connectedPeripheral writeMemoryValue:BM77SPP_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [_connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    else if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]] && add == BLETR_AD) {
        if (d != 0x17) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [_connectedPeripheral writeMemoryValue:BLETR_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [_connectedPeripheral setTransDataNotification:TRUE];
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error {
    if (!error) {
        isPatch = YES;
        [_connectedPeripheral setTransDataNotification:TRUE];
    }
}

//回调函数
- (void)reliableBurstData:(ReliableBurstData *)reliableBurstData didSendDataWithCharacteristic:(CBCharacteristic *)transparentDataWriteChar {
    [self sendPrintData];
    
}

//封装需要打印的数据到缓冲区
- (void)wrapPrintData {
    
    
    [_printer.printerInfo.wrap reset];//缓冲区复位
    [_printer.esc restorePrinter];//
    
//    // 卫生行政执法文书
//    [self.printer.esc.image drawOut:32 y:0 imageWidthDots:106 imageHeightDots:106 mode:IMAGE_ENLARGE_NORMAL imageData:logo_data];
//    [self.printer.esc.text drawOut:150 y:29 height:x24 bold:TRUE enlarge:TEXT_ENLARGE_HEIGHT_WIDTH_DOUBLE text:@"卫生行政执法文书"];
//    for (int i = 0; i < 4; i++) {
//        [self.printer.esc.grahic linedrawOut:0 endPoint:575];
//    }
//    [self.printer.esc feedDots:4];
//    
//    [self.printer.esc.text drawOut:CENTER height:x24 bold:TRUE enlarge:TEXT_ENLARGE_NORMAL text:@"现场检测笔录"];
//    //    [self.printer.esc.barcode code128_auto_drawOut:CENTER unit:BARx3 height:56 pos:BOTTOM size:BAR_ASCII_8x16 str:@"A02161645760"];
//    [self.printer.esc.barcode code128_auto_printOut:LEFT unit:BARx2 height:70 pos:BOTTOM size:BAR_ASCII_8x16 str:@"800SH2015051000054-1"];
//    [self.printer.esc.text drawOut:LEFT height:x24 bold:FALSE enlarge:TEXT_ENLARGE_NORMAL text:@""];
//    [self.printer.esc.text printOut:@"被检查人：上海济强电子科技有限公司"];
//    [self.printer.esc.text printOut:@"联系电话：13371967607  邮政编码：201206"];
//    [self.printer.esc.text printOut:@"法定代表人(或责任人)：张三    职务：总经理"];
//    [self.printer.esc.text printOut:@"检查机关：浦东1队"];
//    [self.printer.esc.text printOut:@"检查时间：yyyy年MM月dd日HH时"];
//    [self.printer.esc.text printOut:@"检查地点：浦东金藏路258号2号楼2层\n"];
//    [self.printer.esc.text printOut:@"检查人员示证检查，检查记录："];
//    [self.printer.esc.text printOut:@"    卫生监督员李四 和王二在赵大的陪同下,对上海济强电子科技有限公司进行了检查，经检查发现："];
//    [self.printer.esc.text printOut:@"1.门外有垃圾。\n2.门口有小广告。\n3.厕所未及时清洁。\r\n"];
//    [self.printer.esc.barcode barcode2D_QRCode:1 ecc:3 text:@"1234567a" length:8];
//    [self.printer.esc feedDots:200];
//    [self.printer.esc.text printOut:@"被检查人阅后签名：\n"];
//    [self.printer.esc.text drawOut:0 y:16 text:@"日期    年  月  日"];
//    [self.printer.esc.text drawOut:280 y:32 text:@"卫生行政机关盖章"];
//    [self.printer.esc.text drawOut:280 y:0 text:@"2015年4月14日"];
//    [printer.esc feedEnter];
//    [self.printer.esc.text printOut:@"卫生监督员签名：\n"];
//    [self.printer.esc.text printOut:@"日期    年  月  日"];
//    for (int i = 0; i < 4; i++) {
//        [self.printer.esc.grahic linedrawOut:0 endPoint:575];
//    }
//    
//    [self.printer.esc feedDots:4];
//    [self.printer.esc.text printOut:RIGHT height:x16 bold:FALSE enlarge:TEXT_ENLARGE_NORMAL text:@"中华人民共和国卫生部制定"];
//    [self.printer.esc feedLines:3];
//    
//    
//    [_printer.esc feedEnter];
    
}




- (void)print:(NSString *)string {
    if(_printer.sendFinish == TRUE)
    {
        return;
    }
    _printer.sendFlag = TRUE;
    [_printer.printerInfo.wrap reset];//缓冲区复位
    [_printer.esc restorePrinter];//
    [self.printer.esc.text printOut:string];

//    [self wrapPrintData];
    if (_printer.printerInfo.wrap.dataLength) {
        _printer.sendFinish = TRUE;
    }
    [self sendPrintData];
    
}
//发送打印数据
-(void) sendPrintData {
    if (!_printer.sendFlag) return;
    if (![_connectedPeripheral.transmit canSendReliableBurstTransmit]) {
        [NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];
        
        return;
    }
    int r = _printer.printerInfo.wrap.dataLength;
    if (r==0) {
        _printer.sendFlag = FALSE;
        _printer.sendFinish = FALSE;
        return;
    }
    
    int sendLength = _printer.port.transmit.transmitSize;
    
    if (r < sendLength) {
        sendLength = r;
    }
    NSData *data = [_printer.printerInfo.wrap getData:sendLength];
    
    [self sendTransparentDataA:data];
    [data release];
}

- (void)sendTransparentDataA:(NSData *)data {
    [_connectedPeripheral sendTransparentData:data type:CBCharacteristicWriteWithoutResponse];
    [NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];
}


@end
