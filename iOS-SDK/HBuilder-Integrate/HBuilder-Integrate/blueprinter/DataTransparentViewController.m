//
//  DataTransparentViewController.m
//  BLEServerTest
//
//  Created by D500 user on 13/1/29.
//  Copyright (c) 2013年 D500 user. All rights reserved.
//

#import "DataTransparentViewController.h"
#import "AppDelegate.h"
#import "ISSCButton.h"

#define BM77SPP_HW @"353035305f535050"
#define BM77SPP_FW @"32303233303230"
#define BM77SPP_AD 0xd97e

#define BLETR_HW @"353035305f424c455452"
#define BLETR_FW @"32303032303130"
#define BLETR_AD 0x4833

@interface NSString (HexString)
+ (NSString *)stringFromHexString:(NSString *)hexString;
@end

@implementation NSString(HexString)

+ (NSString *)stringFromHexString:(NSString *)hexString {
    
    // The hex codes should all be two characters.
    if (([hexString length] % 2) != 0)
        return nil;
    
    NSMutableString *string = [NSMutableString string];
    
    for (NSInteger i = 0; i < [hexString length]; i += 2) {
        
        NSString *hex = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSInteger decimalValue = 0;
        sscanf([hex UTF8String], "%x", &decimalValue);
        [string appendFormat:@"%c", decimalValue];
    }
    
    return string;
}

@end

@interface DataTransparentViewController () <ReliableBurstDataDelegate>{
    NSString *_hardwareRevision;
    NSString *_firmwareRevision;
    BOOL isPatch;
    NSMutableArray *_loopBackQueue;
    NSTimer *_loopBackTimer;
}

@end

@implementation DataTransparentViewController
@synthesize dirArray;
@synthesize connectedPeripheral;
@synthesize comparedPath;
@synthesize checkRxDataTimer;
@synthesize receivedDataPath;
@synthesize printer;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"[DataTransparentViewController] initWithNibName");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      
        content = [[NSMutableString alloc] initWithCapacity:100008];
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        isPatch = NO;
        _loopBackQueue = nil;
        printer=[[Printer alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"[DataTransparentViewController] viewDidLoad");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 28, 57, 57)];
    [titleLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Icon_old"]]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];//aaa
    self.navigationItem.titleView = titleLabel;
    [titleLabel release];
    
    ISSCButton *button = [ISSCButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
    [button addTarget:self action:@selector(saveReceivedData) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Save As" forState:UIControlStateNormal];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    saveAsButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    //saveAsButton = [[UIBarButtonItem alloc] initWithTitle:@"Save As" style:UIBarButtonItemStyleBordered target:self action:@selector(saveReceivedData)];
    button = [ISSCButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
    [button addTarget:self action:@selector(selectCompareFile) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Compare" forState:UIControlStateNormal];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    compareButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    //compareButton = [[UIBarButtonItem alloc] initWithTitle:@"Compare" style:UIBarButtonItemStyleBordered target:self action:@selector(selectCompareFile)];
    button = [ISSCButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
    [button addTarget:self action:@selector(selectTxFile) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@" TX File " forState:UIControlStateNormal];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    txFileButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    //txFileButton = [[UIBarButtonItem alloc] initWithTitle:@" TX File " style:UIBarButtonItemStyleBordered target:self action:@selector(selectTxFile)];
    button = [ISSCButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
    [button addTarget:self action:@selector(clearWebView) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"  Clear  " forState:UIControlStateNormal];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    clearButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    //clearButton = [[UIBarButtonItem alloc] initWithTitle:@"  Clear  " style:UIBarButtonItemStyleBordered target:self action:@selector(clearWebView)];
    /*button = [ISSCButton buttonWithType:UIButtonTypeCustom];
     button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
     [button addTarget:self action:@selector(cancelEditing) forControlEvents:UIControlEventTouchUpInside];
     [button setTitle:@"Cancel" forState:UIControlStateNormal];
     button.titleLabel.adjustsFontSizeToFitWidth = YES;
     cancelButton = [[UIBarButtonItem alloc] initWithCustomView:button];*/
    cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelEditing)];
    button = [ISSCButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0f, 0.0f, 60.0f, 30.0f);
    [button addTarget:self action:@selector(toggleWriteType) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Write Type" forState:UIControlStateNormal];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    writeTypeButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    //writeTypeButton = [[UIBarButtonItem alloc] initWithTitle:@"Write Type" style:UIBarButtonItemStyleBordered target:self action:@selector(toggleWriteType)];
    writeType = CBCharacteristicWriteWithoutResponse;//CBCharacteristicWriteWithResponse;
    
    NSArray *toolbarItems = [[NSArray alloc] initWithObjects:compareButton, txFileButton, writeTypeButton, clearButton, nil];
    self.toolbarItems = toolbarItems;
    [toolbarItems release];

    
    [self.webView setDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self groupComponentsDisplay];
    editingTextField = self.inputTextField;
    fileManager = [NSFileManager defaultManager];
    
    NSString *path = [[NSString alloc] initWithFormat:@"%@/%@.app",NSHomeDirectory(), [[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]];
    NSDirectoryEnumerator *dirEnumerator = [fileManager enumeratorAtPath: path];
    
    dirArray = [[NSMutableArray alloc] init];
    
    NSString *currentFile;
    while (currentFile = [dirEnumerator nextObject]) {
        NSRange range = [currentFile rangeOfString:@".txt"];
        if (range.location != NSNotFound) {
            [dirArray addObject:currentFile];
        }
    }
    if ([dirArray count] == 0) {
        [dirArray addObject:@"No File"];
    }
    NSLog(@"dirarray count = %d", [dirArray count]);
    [path release];
    comparedPath = nil;
    checkRxDataTimer = nil;
    receivedDataPath = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"[DataTransparentViewController] viewDidAppear");
    connectedPeripheral.deviceInfoDelegate = self;
    connectedPeripheral.proprietaryDelegate = self;
    if (isPatch) {
        [connectedPeripheral setTransDataNotification:TRUE];
    }
    else {
        [connectedPeripheral readHardwareRevision];
    }
    connectedPeripheral.transmit.delegate = self;
    [printer setPort:connectedPeripheral];
    [self groupComponentsDisplay];
    webFinishLoad = TRUE;
    [self reloadOutputView];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate navigationController] setToolbarHidden:NO animated:YES];
    if (([connectedPeripheral transparentDataWriteChar] == nil) || ([connectedPeripheral transparentDataReadChar] == nil) ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Service Not Found" message:@"Can't find custom service UUID or TX/RX UUID" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated {
    NSLog(@"[DataTransparentViewController] viewDidDisappear");
    [editingTextField resignFirstResponder];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[appDelegate navigationController] setToolbarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"[DataTransparentViewController] didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)dealloc {
    NSLog(@"[DataTransparentViewController] dealloc");
    if (checkRxDataTimer)
        [checkRxDataTimer invalidate];
    if (receivedDataPath) {
        [fileManager removeItemAtPath:receivedDataPath error:NULL];
    }
    [printer dealloc];
    [_inputTextField release];
    [_segmentedControl release];
    [_timerDeltaTimeTextField release];
    [_timerPatternSizeTextField release];
    [_timerRepeatTimesTextField release];
    [_timerStartButton release];
    [_webView release];
    [_timerDeltaTimeLabel release];
    [_timerPatternSizeLabel release];
    [_timerRepeatTimesLabel release];
    [_timerLabel release];
    [_statusLabel release];
    [saveAsButton release];
    [compareButton release];
    [txFileButton release];
    [clearButton release];
    [cancelButton release];
    [writeTypeButton release];
    [_writeTypeLabel release];
    [_hardwareRevision release];
    [_firmwareRevision release];
    if (_loopBackQueue) {
        [_loopBackQueue release];
        _loopBackQueue = nil;
    }
    if (_loopBackTimer) {
        [_loopBackTimer invalidate];
        _loopBackTimer = nil;
    }
    [super dealloc];
}



- (void)MyPeripheral:(MyPeripheral *)peripheral didSendTransparentDataStatus:(NSError *)error {
    NSLog(@"[DataTransparentViewController] didSendTransparentDataStatus");
    if (error == nil) {
        writeAllowFlag = TRUE;
        if (printer.sendFlag) {
            [NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];
        }
    }
    else if (writeType == CBCharacteristicWriteWithResponse){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Transparent data TX error" message:error.domain delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [alertView release];
        if (printer.sendFlag) {
            printer.sendFlag = FALSE ;}
        if (sendDataTimer) {
            [sendDataTimer invalidate];
            sendDataTimer = nil;
            [self.timerStartButton setTitle: @"Start" forState:UIControlStateNormal];
        }
    }
}

#pragma mark -
#pragma mark Responding to keyboard events
- (void) moveTextViewForKeyboard:(NSNotification*)aNotification up: (BOOL) up{
    NSDictionary* userInfo = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    
    newFrame.origin.y -= (keyboardFrame.size.height-100) * (up? 1 : -1);
    self.view.frame = newFrame;
    
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    int segmentIndex = [self.segmentedControl selectedSegmentIndex];
    if (segmentIndex != CBTimerMode) {
        [self moveTextViewForKeyboard:notification up:YES];
    }
	[self.webView stringByEvaluatingJavaScriptFromString:@"window.scrollTo(document.body.scrollWidth, document.body.scrollHeight);"];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    int segmentIndex = [self.segmentedControl selectedSegmentIndex];
    if (segmentIndex != CBTimerMode) {
        [self moveTextViewForKeyboard:notification up:NO];
    }
	[self.webView stringByEvaluatingJavaScriptFromString:@"window.scrollTo(document.body.scrollWidth, document.body.scrollHeight);"];
}


- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    NSLog(@"[DataTransparentViewController] webViewDidFinishLoad");
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.scrollTo(document.body.scrollWidth, document.body.scrollHeight);"];
    webFinishLoad = TRUE;
}

- (void)reloadOutputView {
    if (webFinishLoad) {
        NSLog(@"[DataTransparentViewController] reloadOutputView");
        webFinishLoad = FALSE;
        NSString *tmp = [[NSString alloc] initWithFormat:@"<html><body>%@</body></html>", content];
        // NSLog(@"html = %@", tmp);
        [self.webView loadHTMLString:tmp baseURL:nil];
        [tmp release];
    }
    
	// TODO implement scrollsToBottomAnimated
}

// <--- UITextFieldDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.3f];
    float width=self.view.frame.size.width;
    float height=self.view.frame.size.height;
    CGRect rect=CGRectMake(0.0f,-80*(textView.tag),width,height);//上移80个单位，一般也够用了
    self.view.frame=rect;
    [UIView commitAnimations];
    return YES;
}

- (BOOL) textFieldDidBeginEditing: (UITextField *)textField
{
    NSLog(@"[DataTransparentViewController] textFieldDidBeginEditing LE");
    editingTextField = textField;
    self.navigationItem.rightBarButtonItem = cancelButton;
    
    return YES;
}
- (BOOL) textFieldDidEndEditing: (UITextField *)textField
{
    NSLog(@"[DataTransparentViewController] textFieldDidEndEditing LE");

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"[DataTransparentViewController] shouldChangeCharactersInRange LE");
    if (textField != self.inputTextField) {
        
        NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
		if ([[string componentsSeparatedByCharactersInSet:unacceptedInput] count] > 1)
			return NO;
		else
			return YES;
    }
    else {
        /*if ([self.inputTextField.text length] > 20) {
            return NO;
        }*/
    }
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
    NSLog(@"[DataTransparentViewController] textFieldShouldReturn LE");
    if (textField == self.inputTextField) {
        self.inputTextField.placeholder = @"";
        [self sendData];
        self.inputTextField.text = @"";
    }
	return YES;
}
// UITextFieldDelegate --->

- (void)sendData {
   // NSLog(@"[DataTransparentViewController] sendData");
    if (![connectedPeripheral.transmit canSendReliableBurstTransmit]) {
        return;
    }
    if ([[self.inputTextField text] length]) {
        [self sendTransparentDataA:[[self.inputTextField text] dataUsingEncoding:NSUTF8StringEncoding]];
        [content appendFormat:@"<span>%@</span><br>", self.inputTextField.text];
        [self reloadOutputView];
    }
}

- (void) groupComponentsDisplay {
    BOOL bRawModeGroup = false, bTimerModeGroup = false;

    if ([self.segmentedControl selectedSegmentIndex] == CBTimerMode) {
        bTimerModeGroup = false;
        bRawModeGroup = true;
    }
    else {
        bTimerModeGroup = true;
        bRawModeGroup = false;
    }
    
    self.webView.hidden = bRawModeGroup;
    self.inputTextField.hidden = bRawModeGroup;
    
    self.timerPatternSizeLabel.hidden = bTimerModeGroup;
    self.timerPatternSizeTextField.hidden = bTimerModeGroup;
    self.timerRepeatTimesLabel.hidden = bTimerModeGroup;
    self.timerRepeatTimesTextField.hidden = bTimerModeGroup;
    self.timerDeltaTimeLabel.hidden = bTimerModeGroup;
    self.timerDeltaTimeTextField.hidden = bTimerModeGroup;
    self.timerStartButton.hidden = bTimerModeGroup;
    
}

- (void)clearLoopBackQueue {
    [_loopBackTimer invalidate];
    _loopBackTimer = nil;
    [_loopBackQueue removeAllObjects];
}

- (IBAction)segmentModeSwitch:(id)sender {
    if (printer.sendFlag) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Transmitting a file" message:nil delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
        [alertView show];
        [alertView release];
        [self.segmentedControl setSelectedSegmentIndex:CBRawMode];
        return;
    }
    [self groupComponentsDisplay];
    int segmentIndex = [self.segmentedControl selectedSegmentIndex];

    if (segmentIndex == CBRawMode) {
        self.timerLabel.text = @"";
        [self reloadOutputView];
        NSArray *toolbarItems = [[NSArray alloc] initWithObjects:compareButton, txFileButton, writeTypeButton, clearButton, nil];
        [self setToolbarItems:toolbarItems animated:TRUE];
        [toolbarItems release];
    }
    else if (segmentIndex == CBLoopBackMode) {
        NSArray *toolbarItems = [[NSArray alloc] initWithObjects: writeTypeButton,clearButton, nil];
        [self setToolbarItems:toolbarItems animated:TRUE];
        [toolbarItems release];
    }
    else if (segmentIndex == CBTimerMode) {
        NSArray *toolbarItems = [[NSArray alloc] initWithObjects:writeTypeButton, nil];
        [self setToolbarItems:toolbarItems animated:TRUE];
        [toolbarItems release];
        
    }
    if ([_loopBackQueue count]) {
        [self clearLoopBackQueue];
    }
    
    if (sendDataTimer)
    {
        [self.timerStartButton setTitle: @"Start" forState:UIControlStateNormal];
        [sendDataTimer invalidate];
        sendDataTimer = nil;
    }
}


- (IBAction)timerButtonAction:(id)sender {
    
}


- (void)sendLoopBackData {
    if (![connectedPeripheral.transmit canSendReliableBurstTransmit]) {
        return;
    }
    else {
        NSData *data = [_loopBackQueue objectAtIndex:0];
        [self sendTransparentDataA:data];
        [_loopBackQueue removeObjectAtIndex:0];
        if ([_loopBackQueue count] == 0) {
            [_loopBackTimer invalidate];
            _loopBackTimer = nil;
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveTransparentData:(NSData *)data {
    NSLog(@"[DataTransparentViewController] didReceiveTransparentData");
    if ([data length] > 0) {
        int segmentIndex = [self.segmentedControl selectedSegmentIndex];
        
        NSMutableString *str = [[NSMutableString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        [str replaceOccurrencesOfString:@"\n" withString:@"<br>" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
        [content appendFormat:@"<span style=\"color:red\">%@</span><br>", str];
        if (segmentIndex == CBLoopBackMode) {
            [content appendFormat:@"<span>%@</span><br>",str];
            if (![connectedPeripheral.transmit canSendReliableBurstTransmit]) {
                if (!_loopBackQueue) {
                    _loopBackQueue = [[NSMutableArray alloc] initWithCapacity:1000];
                }
                if ([_loopBackQueue count]==1000) {
                    [_loopBackQueue removeObjectAtIndex:0];
                }
                [_loopBackQueue addObject:data];
                if (!_loopBackTimer) {
                    _loopBackTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(sendLoopBackData) userInfo:nil repeats:YES];
                }
            }
            else {
                if (_loopBackQueue && [_loopBackQueue count]>0) {
                    if ([_loopBackQueue count]==1000) {
                        [_loopBackQueue removeObjectAtIndex:0];
                    }
                    [_loopBackQueue addObject:data];
                }
                else {
                    [self sendTransparentDataA:data];
                }
            }
        }

        if (checkRxDataTimer == nil) {
            checkRxDataTimer = [NSTimer scheduledTimerWithTimeInterval:CHECK_RX_TIMER target:self selector:@selector(checkRxData) userInfo:nil repeats:YES];
            rxDataTime.trail_time = 0;
            rxDataTime.transmit_time = 0;
            rxDataTime.sinceDate = [[NSDate date] retain];
            lastReceivedByteCount = 0;
            
            receivedByteCount = 0;
        }
        
        if (receivedDataPath == nil) {
            receivedDataPath = [[NSString alloc] initWithFormat:@"%@/Documents/%f.txt",NSHomeDirectory(), [rxDataTime.sinceDate timeIntervalSince1970]];
            if (![fileManager createFileAtPath:receivedDataPath contents:nil attributes:nil]) {
                NSLog(@"Create file fail");
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Create file fail!"  message:@"Create file fail! Cant save recevied file." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alertView show];
                [alertView release];
            }
            else
                NSLog(@"create file %@", receivedDataPath);
        }

        NSFileHandle *fileHandleWrite = [NSFileHandle fileHandleForWritingAtPath:receivedDataPath];
        if (fileHandleWrite == nil) {
            NSLog(@"open file fail");
        }
        rxDataTime.transmit_time = [[NSDate date] timeIntervalSinceDate:rxDataTime.sinceDate];
        receivedByteCount += [data length];
        [fileHandleWrite seekToEndOfFile];
        [fileHandleWrite writeData:data];
        [fileHandleWrite closeFile];
        if ([content length] > 5000) {
            NSRange range = {2500,1000};
            range = [content rangeOfString:@"<span style=" options:NSLiteralSearch range:range];
            if (range.location != NSNotFound) {
                range.length = range.location;
                range.location = 0;
                [content deleteCharactersInRange:range];
            }
            
        }
    }
}

- (void)saveReceivedData {
    
}

- (void)selectCompareFile {
    ISSCTableAlertView  *alert = [[[ISSCTableAlertView alloc] initWithCaller:self data:dirArray title:@"Select a file to compare" buttonTitle:@"Don't compare" andContext:@"Compare"] autorelease];
    [alert show];
}

- (void)selectTxFile {
    ISSCTableAlertView  *alert = [[[ISSCTableAlertView alloc] initWithCaller:self data:dirArray title:@"Tx File" buttonTitle:@"Cancel" andContext:@"TxFile"] autorelease];
    [alert show];
}

- (void)toggleWriteType {
//    if (writeType == CBCharacteristicWriteWithResponse) {
//        writeType = CBCharacteristicWriteWithoutResponse;
//        [self.writeTypeLabel setText:@"Write with ReliableBurstTransmit"];
//    }
//    else {
//        writeType = CBCharacteristicWriteWithResponse;
//        [self.writeTypeLabel setText:@"Write with Response"];
//    }
    
}

- (void)clearWebView {
    NSString *htmlBody = @"<html><body></body></html>";
	NSRange range;
    range.location = 0;
    range.length = [content length];
    [content deleteCharactersInRange:range];
	[self.webView loadHTMLString:htmlBody baseURL:nil];
    //[fileManager removeItemAtPath:receivedDataPath error:NULL];
    if ([_loopBackQueue count]) {
        [self clearLoopBackQueue];
    }
}

-(void)didSelectRowAtIndex:(NSInteger)row withContext:(id)context{
    NSString *tmp = (NSString *)context;
    if ([tmp isEqualToString:@"Compare"]) {
        NSLog(@"DataTransparentViewController] didSelectRowAtIndex Compare");
        if(row >= 0){
            comparedPath = [[NSString alloc] initWithFormat:@"%@/%@.app/%@",NSHomeDirectory(), [[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"], [dirArray objectAtIndex:row]];
            NSLog(@"Did select %@", comparedPath);
        }
        else{
            NSLog(@"Selection cancelled");
            if (comparedPath) {
                [comparedPath release];
                comparedPath = nil;
            }
        }
    }
    else {
        if (row >= 0) {
            
            self.navigationItem.rightBarButtonItem = cancelButton;
        }
        else{
            NSLog(@"Selection cancelled");
            if (printer.sendFlag) {
                printer.sendFlag = FALSE;
            }
        }
        
    }
}

- (void)cancelEditing {
    [editingTextField resignFirstResponder];
    if (printer.sendFlag) {
        printer.sendFlag =FALSE;    }
    self.navigationItem.rightBarButtonItem = nil;
}



- (void)checkRxData
{
    rxDataTime.trail_time++;
    static short refreshOutputViewCount = 0;
    refreshOutputViewCount++;

    if (lastReceivedByteCount < receivedByteCount) {//check if new incoming data
        rxDataTime.trail_time = 0;
        lastReceivedByteCount = receivedByteCount;
        NSLog(@"current time = %f", (rxDataTime.transmit_time));
        
        NSString *tmp = [[NSString alloc] initWithFormat:@"Rx bytes = %d, time = %f",receivedByteCount, rxDataTime.transmit_time];
        self.statusLabel.text = tmp;
        [tmp release];
        if (refreshOutputViewCount > 10) {
            refreshOutputViewCount = 0;
            [self reloadOutputView];
        }
    }
    else if ((refreshOutputViewCount > rxDataTime.trail_time) && (refreshOutputViewCount > 10)){
        refreshOutputViewCount = 0;
        [self reloadOutputView];
    }
    else if (rxDataTime.trail_time >=50) {//no new incoming data over 5 seconds
        NSLog(@"rxDataTime.trail_time >=50..1");
        [checkRxDataTimer invalidate]; //remove timer
        checkRxDataTimer = nil;
        BOOL bResult = TRUE;
        refreshOutputViewCount = 0;
        if (comparedPath && [comparedPath length] != 0 && receivedDataPath) {
            NSLog(@"compare...");
            bResult = [fileManager contentsEqualAtPath:comparedPath andPath:receivedDataPath];
            
            NSString *tmp = [[NSString alloc] initWithFormat:@"Rx bytes = %d,  time = %.3fs,  compare %@",receivedByteCount, rxDataTime.transmit_time, bResult ?@"Pass":@"Fail"];
            self.statusLabel.text = tmp;
            [tmp release];
            if (bResult == false) {

                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Compare Fail"  message:[NSString stringWithFormat:@"Please check /Documents/%@.txt", [rxDataTime.sinceDate description]]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alertView show];
                [alertView release];
            }
            [rxDataTime.sinceDate release];
        }
        if (receivedDataPath) {
            if(bResult)
                [fileManager removeItemAtPath:receivedDataPath error:NULL];
            [receivedDataPath release];
            receivedDataPath = nil;
        }
        NSLog(@"rxDataTime.trail_time >=50..2");
    }
}
- (void)viewDidUnload {
    [self setWriteTypeLabel:nil];
    [super viewDidUnload];
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateTransDataNotifyStatus:(BOOL)notify{
    NSLog(@"DataTransparentViewController] didUpdateTransDataNotifyStatus = %@",notify==true?@"true":@"false");
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateHardwareRevision:(NSString *)hardwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"Hardware = %@",hardwareRevision);
        _hardwareRevision = [hardwareRevision retain];
        if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]]) {
            //BM77SPP
            [connectedPeripheral readFirmwareRevision];
        }
        else if ([hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]]) {
            //BLETR
            [connectedPeripheral readFirmwareRevision];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }

}

- (void)MyPeripheral:(MyPeripheral *)peripheral didUpdateFirmwareRevision:(NSString *)firmwareRevision error:(NSError *)error {
    if (!error) {
        NSLog(@"firmwareRevision = %@",firmwareRevision);
        _firmwareRevision = [firmwareRevision retain];
        if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]]) {
            //BM77SPP
            [connectedPeripheral readMemoryValue:BM77SPP_AD length:2];
        }
        else if ([firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]]) {
            //BLETR
            [connectedPeripheral readMemoryValue:BLETR_AD length:2];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }

}

- (void)MyPeripheral:(MyPeripheral *)peripheral didReceiveMemoryAddress:(NSData *)address length:(short)length data:(NSData *)data {
    NSLog(@"%@ = %@",address,data);
    _writeTypeLabel.text = [NSString stringWithFormat:@"Address %@ = %@",address,data];
    unsigned short int add;
    [address getBytes:&add length:2];
    add = NSSwapBigShortToHost(add);
    unsigned short int d;
    [data getBytes:&d length:length];
    d = NSSwapBigShortToHost(d);
    if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BM77SPP_FW]] && add == BM77SPP_AD) {
        if (d != 0x0017) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BM77SPP_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
    else if ([_hardwareRevision hasPrefix:[NSString stringFromHexString:BLETR_HW]] && [_firmwareRevision hasPrefix:[NSString stringFromHexString:BLETR_FW]] && add == BLETR_AD) {
        if (d != 0x17) {
            char w[2];
            w[0] = 0x00;
            w[1] = 0x17;
            [connectedPeripheral writeMemoryValue:BLETR_AD length:2 data:w];
        }
        else {
            isPatch = YES;
            [connectedPeripheral setTransDataNotification:TRUE];
        }
    }
}

- (void)MyPeripheral:(MyPeripheral *)peripheral didWriteMemoryAddress:(NSError *)error {
    if (!error) {
        isPatch = YES;
        [connectedPeripheral setTransDataNotification:TRUE];
    }
}

//回调函数
- (void)reliableBurstData:(ReliableBurstData *)reliableBurstData didSendDataWithCharacteristic:(CBCharacteristic *)transparentDataWriteChar {
        [self sendPrintData];
   
}

//封装需要打印的数据到缓冲区
- (void)wrapPrintData {
   
    
    [printer.printerInfo.wrap reset];//缓冲区复位
    [printer.esc restorePrinter];//

   // 卫生行政执法文书
    [self.printer.esc.image drawOut:32 y:0 imageWidthDots:106 imageHeightDots:106 mode:IMAGE_ENLARGE_NORMAL imageData:logo_data];
    [self.printer.esc.text drawOut:150 y:29 height:x24 bold:TRUE enlarge:TEXT_ENLARGE_HEIGHT_WIDTH_DOUBLE text:@"卫生行政执法文书"];
    for (int i = 0; i < 4; i++) {
        [self.printer.esc.grahic linedrawOut:0 endPoint:575];
    }
    [self.printer.esc feedDots:4];
  
    [self.printer.esc.text drawOut:CENTER height:x24 bold:TRUE enlarge:TEXT_ENLARGE_NORMAL text:@"现场检测笔录"];
//    [self.printer.esc.barcode code128_auto_drawOut:CENTER unit:BARx3 height:56 pos:BOTTOM size:BAR_ASCII_8x16 str:@"A02161645760"];
    [self.printer.esc.barcode code128_auto_printOut:LEFT unit:BARx2 height:70 pos:BOTTOM size:BAR_ASCII_8x16 str:@"800SH2015051000054-1"];
    [self.printer.esc.text drawOut:LEFT height:x24 bold:FALSE enlarge:TEXT_ENLARGE_NORMAL text:@""];
    [self.printer.esc.text printOut:@"被检查人：上海济强电子科技有限公司"];
    [self.printer.esc.text printOut:@"联系电话：13371967607  邮政编码：201206"];
    [self.printer.esc.text printOut:@"法定代表人(或责任人)：张三    职务：总经理"];
    [self.printer.esc.text printOut:@"检查机关：浦东1队"];
    [self.printer.esc.text printOut:@"检查时间：yyyy年MM月dd日HH时"];
    [self.printer.esc.text printOut:@"检查地点：浦东金藏路258号2号楼2层\n"];
    [self.printer.esc.text printOut:@"检查人员示证检查，检查记录："];
    [self.printer.esc.text printOut:@"    卫生监督员李四 和王二在赵大的陪同下,对上海济强电子科技有限公司进行了检查，经检查发现："];
    [self.printer.esc.text printOut:@"1.门外有垃圾。\n2.门口有小广告。\n3.厕所未及时清洁。\r\n"];
    [self.printer.esc.barcode barcode2D_QRCode:1 ecc:3 text:@"1234567a" length:8];
    [self.printer.esc feedDots:200];
    [self.printer.esc.text printOut:@"被检查人阅后签名：\n"];
    [self.printer.esc.text drawOut:0 y:16 text:@"日期    年  月  日"];
    [self.printer.esc.text drawOut:280 y:32 text:@"卫生行政机关盖章"];
    [self.printer.esc.text drawOut:280 y:0 text:@"2015年4月14日"];
    [printer.esc feedEnter];
    [self.printer.esc.text printOut:@"卫生监督员签名：\n"];
    [self.printer.esc.text printOut:@"日期    年  月  日"];
    for (int i = 0; i < 4; i++) {
        [self.printer.esc.grahic linedrawOut:0 endPoint:575];
    }

    [self.printer.esc feedDots:4];
    [self.printer.esc.text printOut:RIGHT height:x16 bold:FALSE enlarge:TEXT_ENLARGE_NORMAL text:@"中华人民共和国卫生部制定"];
    [self.printer.esc feedLines:3];


    [printer.esc feedEnter];

    }
    
    


- (IBAction)print:(id)sender {
    if(printer.sendFinish == TRUE)
    {
        return;
    }
    printer.sendFlag = TRUE;
    [self wrapPrintData];
    if (printer.printerInfo.wrap.dataLength) {
        printer.sendFinish = TRUE;
    }
    [self sendPrintData];
    
}
//发送打印数据
-(void) sendPrintData {
    if (!printer.sendFlag) return;
    if (![connectedPeripheral.transmit canSendReliableBurstTransmit]) {
        [NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];

        return;
    }
    int r = printer.printerInfo.wrap.dataLength;
    if (r==0) {
        printer.sendFlag = FALSE;
        printer.sendFinish = FALSE;
        return;
    }

//    int sendLength = printer.port.transmit.transmitSize;
    int sendLength = 64;
    if (r < sendLength) {
        sendLength = r;
    }
    NSData *data = [printer.printerInfo.wrap getData:sendLength];
    
    [self sendTransparentDataA:data];
    [data release];
}

- (void)sendTransparentDataA:(NSData *)data {
    [connectedPeripheral sendTransparentData:data type:CBCharacteristicWriteWithoutResponse];
    [NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(sendPrintData) userInfo:nil repeats:NO];
}


  Byte logo_data[] = {0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01,
    0x01, 0x03, 0x03, 0x03, 0x03, 0x03, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x03, 0x03, 0x03, 0x03,
    0x03, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x03, 0x03, 0x07, 0x0F, 0x0F, 0x1F, 0x1E, 0x3F, 0x3D, 0x7C,
    0x7F, 0xFF, 0xF0, 0xF8, 0xFC, 0xFE, 0xEE, 0xE0, 0xFC, 0xFD, 0xFD,
    0xFF, 0xFF, 0xFF, 0xBF, 0x81, 0xEF, 0xAF, 0x81, 0xFF, 0xE1, 0x81,
    0xC5, 0xC9, 0xFD, 0xF9, 0xF3, 0xE0, 0xF8, 0xFE, 0xFC, 0xE1, 0xF7,
    0xFF, 0xFF, 0xFB, 0xFC, 0x7C, 0x7E, 0x3F, 0x3E, 0x1F, 0x1F, 0x0F,
    0x0F, 0x07, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
    0x07, 0x0F, 0x1F, 0x3F, 0x7F, 0x7E, 0xFA, 0xF9, 0xFC, 0xE2, 0xD9,
    0x9D, 0xDD, 0x43, 0x3F, 0x0F, 0x2F, 0x3F, 0x3F, 0xBF, 0x3E, 0xFE,
    0xFC, 0x7C, 0xF8, 0xF8, 0xF8, 0xF0, 0xF0, 0xF0, 0xF0, 0xE0, 0xE0,
    0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xF0, 0xF0,
    0xF0, 0xF0, 0xF0, 0xF8, 0x78, 0x78, 0x7C, 0x7C, 0xFE, 0x1E, 0x7F,
    0xEF, 0xC7, 0x9F, 0x1B, 0xC3, 0x8F, 0xBF, 0xFF, 0xFF, 0xFE, 0xFC,
    0xF9, 0xFD, 0x7E, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x03, 0x0F, 0x3F, 0x7F, 0xFF, 0xFC, 0xFE, 0xF7, 0xF3, 0xB0,
    0x32, 0x33, 0x93, 0xCF, 0xDF, 0x7F, 0x7E, 0xFC, 0xF8, 0xF1, 0xE0,
    0xC0, 0xC0, 0x80, 0x00, 0x01, 0x03, 0x03, 0x07, 0x07, 0x07, 0x07,
    0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04,
    0x07, 0x07, 0x07, 0x03, 0x03, 0x01, 0x00, 0x00, 0x80, 0xC0, 0xE0,
    0xF0, 0xF0, 0xF8, 0xFC, 0x7F, 0x3F, 0x9F, 0xBF, 0x07, 0x04, 0xCE,
    0xD0, 0xF7, 0xF1, 0xFE, 0xFF, 0xFF, 0x7F, 0x1F, 0x07, 0x03, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x01, 0x0F, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF,
    0xDF, 0x9F, 0x8F, 0xAF, 0x27, 0x3F, 0x3F, 0xFF, 0xFE, 0xF8, 0xF0,
    0xC7, 0x01, 0x00, 0x1C, 0x7F, 0xFF, 0x01, 0x01, 0x03, 0x33, 0xF7,
    0xF6, 0xF6, 0xE6, 0xE6, 0xCC, 0x89, 0x02, 0x04, 0x04, 0x08, 0x00,
    0x10, 0x10, 0x00, 0x00, 0x20, 0x21, 0x21, 0x21, 0x21, 0x20, 0x20,
    0x00, 0x10, 0x10, 0x00, 0x08, 0x04, 0x04, 0x02, 0x85, 0xC6, 0xE6,
    0xE6, 0xF7, 0xF7, 0x73, 0x3B, 0x01, 0x00, 0x01, 0xFF, 0x3F, 0x08,
    0x00, 0x83, 0xC3, 0xF0, 0xFC, 0xFF, 0x7F, 0x1F, 0xBB, 0x38, 0x5A,
    0x11, 0xF7, 0xF1, 0xFD, 0xFF, 0xFF, 0xFF, 0x1F, 0x07, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0x9A, 0x9C, 0x8C, 0x85, 0xB7, 0x7F,
    0xFF, 0xFF, 0xF8, 0xC0, 0x07, 0x07, 0x01, 0xF8, 0xFE, 0x1F, 0x03,
    0xE6, 0xCC, 0x9C, 0x9C, 0x39, 0x38, 0x30, 0x62, 0x58, 0x20, 0x40,
    0x80, 0x00, 0x07, 0x07, 0x04, 0x04, 0x04, 0x07, 0x06, 0x03, 0x0F,
    0xFF, 0x30, 0x10, 0x10, 0xF8, 0xFF, 0x0F, 0x03, 0x06, 0x07, 0x04,
    0x04, 0x04, 0x06, 0x07, 0x00, 0x80, 0x40, 0x30, 0x6C, 0x33, 0x38,
    0x19, 0x9C, 0x9C, 0xCC, 0xE6, 0xE3, 0x01, 0x3F, 0xFE, 0xF8, 0x01,
    0x0F, 0x01, 0xC0, 0xFC, 0xFF, 0xFF, 0x4F, 0x92, 0x3E, 0x3E, 0x96,
    0xE0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x24, 0x2D, 0xAC, 0xA9, 0xA1, 0xFF, 0xFF, 0xFF, 0xE3, 0x00, 0x00,
    0x87, 0xF1, 0xF8, 0x7C, 0x1E, 0xC7, 0x8F, 0x38, 0x71, 0x63, 0xE7,
    0xCC, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0xFB, 0x01, 0xFE, 0xFC, 0xF8, 0x00, 0x01, 0x01,
    0x00, 0xF8, 0xFC, 0x02, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x80, 0xE7, 0x63, 0x71,
    0x38, 0x1E, 0xC7, 0x87, 0x3E, 0x7C, 0xF8, 0xE3, 0x0F, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0x6C, 0xEB, 0xEB, 0xEB, 0x09, 0x6E, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xBF, 0xA1, 0x3C, 0xBC,
    0x1D, 0xE1, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0xC0, 0xF3, 0xF1, 0x79,
    0x38, 0x9C, 0x9E, 0x7E, 0xE0, 0x80, 0x0F, 0x7F, 0x1E, 0xC1, 0x10,
    0x02, 0x01, 0x00, 0x00, 0x00, 0xFE, 0xFE, 0x06, 0x06, 0x0A, 0x12,
    0x12, 0x26, 0x44, 0x08, 0x90, 0x30, 0x10, 0x90, 0x08, 0x04, 0x22,
    0x13, 0x03, 0x0B, 0x07, 0x03, 0x03, 0xFE, 0x00, 0x00, 0x00, 0x01,
    0x04, 0x31, 0x04, 0x7F, 0x1F, 0x80, 0xE0, 0x7F, 0x1E, 0xCE, 0x1C,
    0x3C, 0x78, 0xF1, 0xE0, 0x80, 0x01, 0x1F, 0xFF, 0xFF, 0xFD, 0x49,
    0x29, 0xA5, 0xA4, 0x2F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0,
    0xF8, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0xFC, 0xF9, 0xF9,
    0xFC, 0xFF, 0xFE, 0x3F, 0x0F, 0x83, 0xC0, 0xE8, 0x66, 0x77, 0x73,
    0x33, 0x13, 0xF9, 0xF9, 0x05, 0xF0, 0xFC, 0x7F, 0x1F, 0x81, 0x00,
    0x20, 0x00, 0x08, 0x04, 0x04, 0x02, 0x02, 0x00, 0x01, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x02, 0x02, 0x04,
    0x00, 0x08, 0x10, 0x20, 0x41, 0x8F, 0x3F, 0xFC, 0xF8, 0x00, 0xFC,
    0xF9, 0x19, 0x19, 0x33, 0x73, 0x76, 0x74, 0xE0, 0xC1, 0x03, 0x0F,
    0x7E, 0xFC, 0xFC, 0xFE, 0xFE, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFE, 0xF8, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0,
    0xF8, 0xFE, 0xFF, 0xE7, 0x4F, 0x0F, 0x8F, 0x03, 0x31, 0x03, 0x63,
    0xF7, 0xFF, 0xFF, 0x7F, 0x3F, 0x1F, 0x8E, 0x87, 0xC3, 0xD1, 0xD8,
    0xDC, 0xCC, 0xCE, 0xCE, 0xCE, 0xCE, 0x4E, 0x1E, 0x1E, 0x1E, 0x1E,
    0x1E, 0x1C, 0x1C, 0x19, 0x1A, 0x14, 0x14, 0x08, 0x08, 0x18, 0x18,
    0x38, 0x38, 0x39, 0x3D, 0x3C, 0x1E, 0x1E, 0x1E, 0x1F, 0x0F, 0x4F,
    0xCF, 0xCF, 0xCF, 0xCF, 0xCE, 0xCE, 0xCC, 0xC8, 0xD1, 0xC3, 0x87,
    0x0F, 0x1F, 0x3F, 0xFF, 0xEF, 0xC1, 0x81, 0x01, 0x01, 0x01, 0x00,
    0x00, 0x00, 0x03, 0x8F, 0xDF, 0xFC, 0xF8, 0xE0, 0x80, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0,
    0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF, 0xFF, 0xFF, 0xF9, 0xF1, 0xC0,
    0x88, 0x04, 0x00, 0x00, 0x80, 0xC1, 0xC0, 0xE0, 0x71, 0x3F, 0x3F,
    0x1F, 0x1F, 0x0F, 0x0F, 0x07, 0x07, 0x07, 0x43, 0xC3, 0x83, 0x03,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x83,
    0xC3, 0x63, 0x07, 0x07, 0x07, 0x0F, 0x0F, 0x0F, 0x1F, 0x1F, 0x3F,
    0x39, 0x70, 0xE2, 0xF9, 0xC8, 0xC0, 0xC6, 0xD4, 0x90, 0xF9, 0xFC,
    0xFE, 0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFC, 0xF8, 0xF0, 0xE0, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x80, 0xC0, 0xC0, 0xE0, 0xF0, 0x78, 0x38, 0x0C, 0x0C,
    0x1E, 0x3F, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFD, 0xFD, 0x7C,
    0x7C, 0x38, 0x00, 0x0E, 0x3A, 0x32, 0x82, 0x8E, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xF7, 0x87, 0x8B, 0xCB, 0x03, 0x00, 0xD0,
    0xD2, 0x92, 0xDE, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x1F,
    0x02, 0x06, 0x0C, 0x9C, 0x18, 0x30, 0x70, 0x60, 0xC0, 0x80, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x80, 0x80, 0x80, 0xC0, 0xC0, 0xC0, 0xE0, 0xE0, 0xE0, 0xE0, 0xF0,
    0xF0, 0x70, 0x70, 0x70, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0,
    0xF0, 0x70, 0x70, 0x70, 0x70, 0x70, 0x60, 0x60, 0xE0, 0xE0, 0xC0,
    0xC0, 0xC0, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00,
};


@end
